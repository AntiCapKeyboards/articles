# Rappel du modèle des ESN

Le milieu de la tech est plutôt bien portant, et sans pouvoir généraliser on peut dire que le marché du travail est globalement en notre faveur (je parle notamment des personnes qui développent); même en cette année 2020 troublée.

Cependant, certaines entreprises n'hésitent pas à attaquer leurs salariés plutôt que de les soutenir en cette période compliquée pour toutes et tous, peut-être est-ce votre cas. Dans le cadre d'un guide d'autodéfense des salariés, voici un rappel de ce qu'est une ESN/SSII.

Ce rappel me semble nécessaire, car notre métier de consultants peut se faire sous le statut de salarié, ou sous celui d'indépendant. L'un et l'autre ne convenant pas à tous le monde. Mais si l'on fait le choix de travailler en SSII, autant le faire pour les bonnes raisons, et en en tirant les bénéfices associés.

## Qu'est-ce qu'une ESN/SSII ?

Une société de service en informatique, si l'on prends le cas de la régie (le métier principal de la plupart des SSII), est construit sur le modèle suivant:

Vous et vos compétences êtes embauché.e.s en CDI, avec un salaire, afin d'être placés chez un client pour un certain prix journalier (TJM).

Votre tarif à la journée est constitué de :
- votre salaire (net, cotisations sociales, cotisations patronales, avantages)
- la marge de la SSII

Je vous conseille de considérer que la marge de la SSII représente ce que vous payez pour un service que vous rends l'ESN. Ce point sera cependant à nuancer plus tard dans l'article.

## Services rendues par l'ESN

L'entreprise est là pour vous rendre plusieurs services, dont les plus basiques sont:
- la compétence commerciale (pour vous trouver une mission et négocier votre TJM)
- la prise de risque: Si vous n'avez pas de mission, l'ESN continue de vous payer quand même, sa marge et l'aspect collectif servent à couvrir le risque

On peut y ajouter divers choses suivant les boites, tel qu'une communauté, des gens qui suivent votre carrière, un CE, mais aussi des choses qui vous impactent plus ou moins directement suivant les cas, comme des locaux, la vision des patrons (via les salaires de vos patrons; Les dividendes sont, elles, une rémunération liée à leur status de propriétaire privé), l'image de la boite qui peut vous aider à trouver des missions (la com, personal branding de votre CTO), le recrutement, ect.

Il peut y avoir d'autres rentrées d'argent pour financer cela, comme de la fraude au CIR (Crédit Impôt Recherche), du CICE, des formations financées par pôle emploi, ect.

Votre entreprise décide donc chaque année de ce qu'elle fait avec l'argent issue de la marge de votre prestation, certaines dépenses sont lié à un support vous étant utile, d'autres sont là pour augmenter les bénéfices de l'entreprise.

La question est donc de se demander chaque année si le service rendu par votre entreprise vaut bien ce qu'il vous coûte (par rapport à l'indépendance par exemple).

Après, il faut comprendre que chaque entreprise est très différente de l'autre, et que ce propos peut être nuancé; Par exemple si votre boite vous permet d'être placés 200€ plus cher par jours que si vous étiez indépendants, cet argent est peut-être plus légitime à être utilisé pour développer l'entreprise indépendamment de vos besoins.
Cela signifie que le service commercial rendu par l'entreprise est efficace.

A contrario, si vous êtes facturés en dessous de 500€ avec 5 ans d'expérience en région parisienne, et qu'on refuse de vous augmenter en raison de votre TJM, alors considérez que le service rendu est médiocre. Il n'est pas rare de voir  des commerciaux incapables de négocier un TJM, et vous demander de faire leur travail pour au final gagner 2x votre salaire: c'est inacceptable.

En résumé, regardez combien vous coûte votre entreprise, et le cas échéant n'hésitez pas à dire à vos patrons qu'ils vous coûtent trop cher.

## Faites vos calculs

Le calcul est trop approximatif pour refléter la réalité, mais ça reste une base de travail intéressante que vous pourrez affiner.

Prenez votre TJM, multipliez le par 18 pour avoir votre chiffre d'affaire par mois. Faites en la valeur cumulée mois après mois.

Faites un graphique avec les mois de l'année sur l'axe des abscisses, et le chiffre d'affaire cumulé sur l'axe des ordonnés. 

Tracez un premier trait horizontal au niveau de votre net annuel (votre brut annuel divisé par 1,20), et un autre trait à votre chargé patronal annuel (votre brut annuel multiplié par 1,40). 

Vous pourrez ainsi voir jusqu'à quel mois dans l'année vous travaillez pour vous, ensuite jusqu'à quand vous travaillez pour la collectivité, puis enfin pour votre boite.
Si vous n'avez pas les informations pour faire ce calcul (le TJM), alors il y a un grand manque de transparence dans votre boite.

## Conclusion
L'idée n'est pas de critiquer le fait de marger sur vos prestations, car c'est le contrat de départ et nous l'avons toutes et tous accepté. 

Cependant, se réapproprier les chiffres, remettre des faits au centre de la discussion, permet de revenir à des rapports sains.

Parfois vous vous rendrez compte que votre entreprise remplit bien son rôle, parfois cela vous permettra de voir que l'on se moque de vous.
