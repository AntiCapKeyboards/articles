# Riposte Syndicale en ESN

Le milieu de la tech est plutôt bien portant, et sans pouvoir généraliser on peut dire que le marché du travail est globalement en notre faveur (je parle notamment des personnes qui développent); même en cette année 2020 troublée.

Cependant, certaines entreprises n'hésitent pas à attaquer leurs salariés plutôt que de les soutenir en cette période compliquée pour toutes et tous, peut-être est-ce votre cas. 

Voici un modeste guide contenant quelques pistes pour vous défendre.

Ce guide s'adresse donc aux salariés du privé, particulièrement ceux travaillant en société de service.

[Rappel du modèle économique des ESN](modele-esn.md)

[Que faire si on vous demande de partir](départ-non-souhaité.md)

[Quelques conseils stratégiques](stratégie.md)

Si vous avez des récits, des expériences ou des corrections à apporter n'hésitez pas à me contacter.



