# Stratégie de négociation

Vous avez reçu un coup de téléphone de votre patron ou de votre RH, et on vous demande de partir, ou de solder tous vos congés en inter-contrat, ect.
Cela ne vous plait guère, et vous souhaitez vous défendre: que faire ?

## Les écrits restent
Envoyez un mail après chaque échange oral, pour résumer la conversation, 
La plupart du temps, lorsque que la direction communique avec vous par oral, c'est parce qu'elle sait qu'elle flirte avec l'illégal.
On va essayer de vous faire consentir à leur projet, pour ne pas avoir à vous forcer.

- on vous demande de poser des congès ? Ces congés doivent être officiellement imposés (il y a un cadre juridique)
- On vous a expliqué que vous n'aurez pas telle mission/augmentation/formation car vous étiez gréviste (c'est illégal), demandez une trace écrite.

Forcer les traces écrite vous aide à préparer un dossier, et ils le savent. Ils savent aussi que vous connaissez sans doute vos droits.
Normalement cela devrait remettre les échanges dans un cadre, ce qui est plus sain pour tout le monde.

## Faites vous accompagner

Si vous êtes convoqués à un entretien suite à un conflit, que ce soit un entretien préalable ou non, faites vous accompagner.

Si vous savez qu'un entretien banal (entretien annuel par exemple) va mal se passer, faites vous accompagner aussi.

Un témoin aide aussi à recadrer les débats. Cela ré-équilibre aussi le rapport de force (si vous êtes face à plusieurs personnes).
Si la personne qui vous accompagne s'y connait en droit du travail, elle va pouvoir vous aider en plus d'être témoins.

### Mon CSE est tout pourris 
Vous êtes cadre, dans une SSII qui vous propose des soirées open-bar, vous avez même fait un machin-challenge l'autre fois c'était marrant...vous ne vous êtes pas vraiment intéréssé à cette histoire d'élection des délégués du personnel.
ça avait l'air chiant, et puis vos patrons, vous les tutoyez, il y a une bonne ambiance, qu'est-ce qui pourrait mal tourner ?

Et maintenant que vous avez des problèmes, vous vous rendez compte que le Comité Social et économique est composé de gontran le commercial, thomas qui joue au golf avec le patron, nadine qui mange tous les midi avec la RH et jean qui n'aime pas le conflit.

Ils organisent de chouettes soirées de nöel, mais ils vont avoir du mal à vous aider.

C'est pour cela que même si ce n'est pas dans notre culture, même si le milieu a une image sympa, même si les plans économique et sociaux ne sont pas monnaie courante en ESN, il est quand même important de s'intéresser à vos représentants, et veiller à ne pas élire n'importe qui.
 
Mais ne paniquez pas, vous n'êtes pas obligé de faire appel à eux. 

### Qui peut vous accompagner ?
Cela va dépendre des cas, et des types d'entretiens.
Mais quoi qu'il arrive, on peut se faire assister d'un·e salarié·e de l'entreprise au choix.

Il y a cependant des profils plus pertinents que d'autres :
- Délégué du personnel, ou membre du CSE: c'est un rôle qu'il a choisi d'avoir, il a du temps dédié à ça (à prendre par demi journées dans la convention synthec si c'est un·e consultant·e), vous l'avez élu.
- Délégué syndical (salarié de l'entreprise). Il a une structure pour l'épauler et le conseiller, il est censé s'y connaitre un peu mieux que la moyenne.
- N'importe qui de confiance dans l'entreprise, de préférence syndiqué·e 

Dans le cas où on parle "d'assistance du salarié" (dans un entretien préalable au licenciement), il est possible de faire appel à quelqu'un d'extérieur. La liste est fournie par la mairie, l'inspection du travail ou les permanences syndicales.

### Absence de CSE

Si votre entreprise n'a pas de CSE alors qu'il devrait, faite une demande officielle par écrit pour que des élections aient lieux.
Ils seront obligés de l'organiser afin de ne pas rentrer dans le délit d'entrave.

>Tous les employeurs de droit privé, quels que soient leur forme juridique, ainsi que certains établissements du secteur public doivent organiser les élections du Comité social et économique (CSE), dès lors qu’ils emploient au moins 11 salariés. Cet effectif doit être atteint pendant 12 mois consécutifs. L’élection de la délégation du personnel au CSE peut avoir lieu par vote électronique si un accord d’entreprise, ou, à défaut l’employeur, le décide.

source: https://travail-emploi.gouv.fr/dialogue-social/le-comite-social-et-economique/article/cse-election-de-la-delegation-du-personnel

Rendez votre demande publique (au sein de l'entreprise).


## Droit de gréve

Dans le cas où la problématique serait collective, ou que plusieurs personnes soutiendrait une autre, la grève est une option.

[Prenez connaissance de votre droit de grève](droit-de-greve.md)

### Droit de grève, bonne ou mauvaise idée ?
Cela va dépendre de la situation bien entendu, et du nombre de personnes impliquées.

Par exemple, si votre problématique est liée au fait que votre client vous a sortis de mission ou qu'il vous a demandé de facturer moins de jours, vous mettre en grève, non rémunérée donc, va l'arranger.
Sauf si des employés en mission (qui facturent donc) se joignent à la grève par solidarité.


## Délit d'entrave

Dans une entreprise, c'est l'infraction d’un employeur qui porte atteinte à la mise en place et au bon déroulement de la mission des représentants du personnel et à l’exercice du droit syndical.
Comme, par exemple:
- répression suite à une grève légale 
- refus d'organiser l'election du CSE
- entrave au bon fonctionnement du CSE

Pour qu'il y ai délit d'entrave, il faut 3 éléments :
- L’élément légal, un ou plusieurs textes de loi qualifiant l’infraction
- L’élément matériel, l’action ou inaction ayant lésé la représentation du personnel
- L’élément moral, l’intention de nuire ou la conscience d’avoir pu entraver le CSE

Le patron ayant fait entrave encoure une amende et 1 an d'emprisonnement.

source: https://www.cseofficiel.fr/delit-entrave-cse-definition-recours/

N'hésitez pas à aborder le sujet, en cas

## Ne restez pas isolé

La principale stratégie des patrons de SSII, c'est de vouloir tout gérer par cas individuel.
On vous demande de ne pas parler de votre augmentation, de ne pas aller voir le CSE, de gérer ce problème "en bonne intelligence".
Cela joue en votre défaveur: vous n'êtes sans doute pas le premier à avoir cette revendication ou à recevoir une invitation à "aller vers d'autres horizons".

Parlez aux autres consultants de votre entreprise, faites des groupes de discussions.
Commencez par prendre contact avec vos collègues travaillant chez le même client que vous, ils auront sans doute les même problèmes !

à plusieurs, on est plus fort.