# Départ non souhaité


NOTE: cet article ne tiens pas compte des ajustements liés au COVID-19


## TL;DR
- On va chercher des infos sur https://www.service-public.fr et là aussi https://code.travail.gouv.fr/outils
- Essayer de remettre la discussion sur le bon niveau de procédure
- La seule façon de perdre son allocation chômage est de démissionner
- Seul les licenciement pour faute grave et lourde font perdre les indemnités de licenciement et de compensation de préavis
- Tous le monde veut éviter d'aller aux prudhommes, eux comme vous
- A la fin, vous assumerez seul·e·s vos décision. donc uniquement vous pouvez décider de vos choix

## Vous êtes en inter-contrat (ou pas) et on vous demande de partir

Le patronat (oui ça fait un peu "lutte ouvrière" comme formulation, mais ça reste plus factuel que quand votre patron vous appelle "collaborateur") tente toujours de décaler d'un cran le dispositif : si vous êtes en désaccords avec eux ils vont vouloir que vous posiez votre démission, et s'ils veulent vous congédier ils vous proposeront une rupture conventionnelle.

## On vous demande de poser votre démission
Dites non. Poser sa démission, c'est **renoncer à l'indemnité de licenciement et à vos droits à l'allocation chômage** (sauf cas très spécifiques considérés comme légitime, mais il y a peu de chance que vous rentriez dans ce cadre si l'on vous force la main : départ pour suivre son conjoint par ex).
 
Si vous choisissez quand même cette option, sachez que vous êtes éligible aux indemnité compensatrice de préavis et aux indemnités de congés payés.
 
L'employeur doit cependant vous remettre votre attestation pôle emploi avec votre solde de tout compte, des fois que vous auriez envie de recevoir les mails pour des formations "bigdata scientist fullstack" et autres "apprenez les métiers du digital", ou pour rentrer dans la gendarmerie nationale. (le droit à une inscription pôle emploi permet de chercher un travail, même si on est pas éligible à l'allocation chômage)
 
## On vous propose une rupture conventionnelle 
La rupture conventionnelle est, comme son nom l'indique, un départ où les 2 parties sont d'accord. La rupture conventionnelle **donne droit à une indemnité de licenciement et à l'allocation chômage**.

Alors pourquoi pas, peut-être que c'est la fin d'une collaboration qui ne sera plus fructueuse pour personne, et qu'il est temps de partir. Cependant, rien ne vous oblige à la signer, et je vous conseille de regarder en détails la proposition:

**L'indemnité de licenciement**: Allez faire une simulation du minimum légal que l'entreprise doit vous verser ( https://code.travail.gouv.fr/outils). 

Si vous n'aviez pas l'intention de partir et êtes en désaccords, je vous conseillerais de ne pas accepter si on ne vous propose pas un multiple de cette somme. Sinon autant se faire licencier.

à noter que tout ce que vous toucherez en supra-légal (au dessus du montant légal) aura un impact sur votre allocation chômage en jours de carences, plafonnées par type d'indemnités: licenciement, congés payés et compensatrice de préavis le cas échéant.

**L'entretien préalable**: Notez qu'il est possible que l'on vous fasse cette proposition lors d'un entretien préalable, auquel vous avez été convoqué·e avec une "convocation à entretien préalable". 

Si c'est le cas et que vous vous demandez "préalable à quoi ?", sachez que c'est "préalable au licenciement". Dans ce cas là, c'est qu'une procédure de licenciement est déjà en cours. Faites vous accompagner (DP, CSE, syndicat, un collègue au choix, quelqu'un d'externe présent sur la liste de l'inspection du travail) et ressortez de l'entretien en sachant bien ce qui vous ai reproché, cela vous permettra de connaître votre marge de manœuvre et voir s'il n'est pas préférable d'accepter la rupture conventionnelle. 

En résumé, vous allez finir par partir de l'entreprise à court/moyen terme.
Ce qui nous emmène au prochain paragraphe.

## On vous licencie pour motif personnel

C'est le licenciement qui vous cible vous en tant que personne salariée. 
Pour licencier un salarié pour motif personnel, que le motif soit disciplinaire (faute) ou non (absence de résultat), il faut une "cause réelle et sérieuse", et cette cause doit reposer sur des **faits réels**, être **précise et vérifiable** et être **suffisamment importante**.

source: https://www.service-public.fr/particuliers/vosdroits/F2835

**Quel que soit le motif, un licenciement ne vous prive pas de vos droits à l'allocation chômage**. 

![this](https://media.giphy.com/media/6qyGyVyYyCwFy/giphy.gif)

source: https://www.service-public.fr/particuliers/vosdroits/F14860. Mais il faut avoir des droits (avoir travaillé au moins 6 mois, donc 130 jours ou 910 heures, dans les 24 derniers mois à la date de fin de votre contrat de travail).

### a. Licenciement pour faute simple
à peu près tout peut rentrer là-dedans, du moment que ça correspond aux critères donnés si dessus. 
Ce motif donne droit à tout: indemnités de licenciement, indemnités de congés payés, indemnités compensatrice de préavis.

### b. Licenciement pour faute grave

La faute grave est celle qui provoque des troubles sérieux ou des pertes pour l'entreprise et rend impossible le maintien du salarié dans l'entreprise, même temporairement. 

On ne licencie pas quelqu'un pour faute grave sans avoir des éléments solides, mais on peut vite tomber dedans si l'on a l'absence ou l'injure facile.

Le refus d'effectuer une tâche prévue dans son contrat de travail peut être un motif, d'où l'importance de bien connaître sa fiche de poste.

En tant que cadre consultant nous n'avons pas vraiment de fiche de poste, cela peut jouer en notre faveur, ou pas. 

Je pense que le refus répété de mission peut rentrer dans ce cadre. 
Le vol, la violence et l'ivresse rentrent aussi dans ce cadre. 

C'est ce motif qui est utilisé en cas d'abandon de poste.

Lors d'un licenciement pour faute grave, aucun préavis ni indemnité de licenciement ne sont dues. Seul les indemnités de congés payés peuvent être versés.

### c. Licenciement pour faute lourde

La faute lourde a toutes les caractéristiques de la faute grave, renforcée par l'intention du salarié de nuire à l'employeur ou à l'entreprise (vol, détournement de fonds, détournement de clientèle, ). 

Il est très peu probable que vous rentriez dans ce cas sans vous en douter. Cependant, le sabotage (dégradation volontaire de l'outils de travail), toute violence physique ou séquestration à l'encontre de l'employeur, ou le blocage de l'accès à l'entreprise aux salariés non grévistes par des salariés grévistes rentrent dans ce cadre.

Elle ne prive pas non plus de l'indemnité de congés payés. à noter qu'au delà de ne pas percevoir certaines indemnités, il est **possible de devoir verser à l'employeur des dommages et intérêts**.

source: https://www.service-public.fr/particuliers/vosdroits/F1137

source: https://travail-emploi.gouv.fr/droit-du-travail/la-rupture-du-contrat-de-travail/article/le-licenciement-pour-motif-personnel-les-causes-possibles-les-sanctions

## On vous licencie pour motif économique

Ce licenciement ne vous cible pas vous personnellement, il est lié à la santé économique de l'entreprise, ou plus précisément à une baisse des commandes ou du chiffre d'affaire.

Si l'entreprise rentre dans ces critères, elle peut licencier des salariés, à partir de 1.

Les critères sont décrits ici: https://www.service-public.fr/particuliers/vosdroits/F2776

A noter qu'en faisant partir des gens au cours de l'année 2020 (RC, départs volontaires, licenciements), une ESN pourra justifier d'une baisse d'activité sur ces prochains trimestres et pourra invoquer un licenciement économique fin 2021 ou début 2022. D'ailleurs, le critère étant le chiffre d'affaire, il est possible de continuer à faire des bénéfices grâce au chômage partiel mais tout en ayant une baisse du chiffre d'affaire. Je n'ai pas eu vent de licenciement économique dans les ESN pour l'instant, mais il se pourrait qu'on les voient fleurir l'année prochaine ou la suivante.

Si vous et votre entreprise rentrez dans les critères du CSP, ils sont obligés de vous proposer un contrat de sécurisation professionnelle (CSP).

C'est extrêmement avantageux pour vous, car cela augmente votre allocation chômage, et vous donne droit à une prime de reclassement.

- source: https://travail-emploi.gouv.fr/emploi/accompagnement-des-mutations-economiques/csp
- source: https://www.service-public.fr/particuliers/vosdroits/F13819
- source: https://www.pole-emploi.fr/candidat/mes-droits-aux-aides-et-allocati/a-chaque-situation-son-allocatio/quelle-est-ma-situation-professi/je-perds-ou-je-quitte-un-emploi/je-suis-licenciee-pour-raison-ec.html


## Quels enjeux ?
Maintenant que vous avez une meilleure image des différentes modalités de départ, quel est l'enjeu de la négociation ?

Pour rappel, nous parlons d'une situation où l'on vous pousse au départ. De votre côté, Il dépend bien entendu de la situation de chacun.e, et de ce que l'on trouve acceptable.

Du côté patronal, l'enjeu va être multiple.

### ESN et Rupture conventionnelle

Certaines ESN n'aiment pas avoir recours en temps normal à la rupture conventionnelle. Outre l'indemnité de licenciement, cela vous permet aussi de passer freelance plus facilement, ce qui ferait de vous un concurent. 

Autant, généraliser la RC ressemblerait à de la fraude au chômage, autant cette approche appliquée de façon dogmatique renvoi surtout à un manque d'intelligence commerciale, mais chacun est libre de sa politique interne. 

J'ai entendu parler d'entreprises qui étaient strictement contre les ruptures conventionnelles, et qui se mettent aujourd'hui à en proposer avec insistance à leurs consultants en inter-contrat. 

Je vous renvois à [l'article sur le modèle des ESN](modele-esn.md) si vous ne voyez pas le problème à ne pas être supporté lorsque vous êtes en inter-contrat. On risque donc de vous proposer une rupture conventionnelle dans le cas où l'on souhaites vous faire partir. 

Mais pourquoi ?

### Éviter les prudhommes 
Vous licencier, c'est s'exposer à des prudhommes. Si le départ se fait via une rupture conventionnelle, il est plus compliqué de revenir dessus pour vous. 

C'est pour cela qu'il est préférable qu'elle soit emballée avec un beau paquet cadeaux: l'indemnité de licenciement est une sorte d'achat de votre silence. 

Se retrouver au chômage, en période de crise, alors que vous payez votre ESN depuis des années pour assurer le risque, ça s'achète. vous n'avez pas envie de revenir sur cette histoire et de passer 3 ans à attendre le résultat d'un prudhommes, **mais eux non plus**. Cela joue aussi en votre faveur.

**Calcul de votre indemnité**: faites une simulation sur [ce site](https://code.travail.gouv.fr/outils/indemnite-licenciement) pour connaître le minimum légal. Sachez que la somme perçue en sus (supra-légal) donne lieux à un délai de carences pôle emploi. Il faut diviser le montant supra-légal par 90, cela donne le nombre de jours de carence. Ce délai est plafonné à 150 jours (75 jours en cas de licenciement économique). Cela signifie que jusqu'à une certaine somme (plafond de jours de carence), cet argent sera juste à considérer comme une extension de vos droits chômages.

source: https://allocation-chomage.fr/delai-carence-pole-emploi/

### Que vous partiez vite
On veut vous faire partir alors que vous êtes en inter-contrat ? parfait !

Plus vous restez en inter-contrat plus vous leur coûtez cher. Faire durer les négociation pour ne pas vous verser d'indemnités leur coûtera de l'argent. Vous avez tout votre temps, surtout si en ce moment les clients ne se bousculent pas. Pour plusieurs raison (refus de créer un précédent, moyens financiers, idéologie, orgueil) cela peut durer longtemps.

## Et vous ?
De votre côté, ne leur donnez pas du grain à moudre pour un licenciement, faites vous conseiller, restez un bon employé, laissez des traces écrites, ect. Et guettez leurs faux pas.

Et pensez à vous faire accompagner lors des entretiens, ça devrait faire redescendre le ton et mieux cadrer les échanges.

## Vos choix

Vous êtes seul·e à la fin à assumer les choix que vous ferez, il vous appartient donc de suivre ou non les conseils que vous avez trouvés ici.

Même si l'article est sourcé, je ne suis pas juge aux prudhommes ni DRH.

Même si le ton de l'article se veut impératif, il ne s'agit en rien d'injonctions, juste une volonté de vous motiver à défendre vos droits. 

Préservez-vous, courage et bonne chance à vous

[retrouvez ici quelques conseils stratégiques](stratégie.md)