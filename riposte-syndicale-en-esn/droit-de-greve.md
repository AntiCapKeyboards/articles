# Droit de grève

Le droit de grève est assez méconnus en France, surtout dans le privé, et encore plus chez les status cadres.

Pour lancer une grève, vous n'avez:
- **pas besoin** d'être syndiqué
- **pas besoin** d'être élu au CSE (ex délégués du personnel)
- **pas besoin** de prévenir à l'avance
- **pas besoin** d'être un grand nombre

Cependant, pour être qualifié de grève, est donc être licite, il faut:
- être **minimum 2** à faire grève (sauf si vous êtes le seul salarié)
- Un arrêt total du travail
- Un arrêt collectif du travail par l'ensemble des salariés grévistes
- Des revendications professionnelles, donc internes à l'entreprise (mais sans pour autant être très précis: augmentation des salaires est un motif légitime)
- Que le patron sache pourquoi vous faites grève, et ce dès le début de la grève (un simple mail suffit).

je ne parle pas ici des grèves portées au niveau national (par un syndicat représentatif), qui permettent, elles, de faire grève seul dans l'entreprise, et de le faire pour des motifs politiques.

Le sabotage par exemple (éteindre un server, supprimer un repo git, passer sa journée en pause café, refuser d'effectuer une astreinte, ect) ne rentrent pas dans le cadre du droit de grève.

Notez qu'un jour de grève n'est pas payé (sauf si ce paiement est négocié avec le patron).

Le patron n'a aucunement le droit de faire apparaitre l'exercice du droit de grève sur le bulletin de paie du gréviste.


source: https://www.service-public.fr/particuliers/vosdroits/F117
